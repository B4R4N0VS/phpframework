<?php
    $connect = mysqli_connect('std-mysql', 'std_1726_framework', '12345678', 'std_1726_framework');
    if(!$connect){
        die("Connection error!");
    }
   
?> 

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<header class="border-bottom">
            <div class="container navbar mt-2">
                <p><?=$_COOKIE['user']?></p>
                <a class="btn btn-primary" href="/articles.php">Назад</a>
            </div>
    </header>
    <main>
        <div class="container mt-5 continer-my">
            <div class="row col-12">
                <img class="col-5" src="img/img2.jpg" alt="">
                <div class="col-7">
                    <h2>Вторая статья</h2>
                    <p>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Nemo perferendis dolorum in iste iusto temporibus provident ratione perspiciatis culpa, alias deleniti voluptas tenetur nihil, excepturi harum sequi, eveniet atque delectus!
                        Eius ratione, modi corporis quod officia libero suscipit iure exercitationem et neque voluptate dolorem iste cum sed consequuntur enim debitis? Laudantium quo dolores voluptates consequuntur reprehenderit distinctio omnis quae in?
                        Cupiditate blanditiis est, at nemo soluta eos illo ullam quas libero ratione eaque, aliquam eius nostrum velit nulla suscipit eum mollitia hic dicta dignissimos impedit vel! Ratione incidunt repudiandae assumenda?
                        Numquam, itaque nemo alias delectus cum illo minima debitis deleniti unde sequi aut aspernatur, sint ad culpa eaque illum nobis, ducimus quidem. Eos earum qui obcaecati corrupti cumque doloribus aliquid!
                    </p>
                </div>
            </div>
        </div>
        <div class="container mt-5">
            <div class="row justify-content-center">
                <form class="col-4 text-center" method="post" action="/add-message2.php">
                    <div class="mb-3">
                        <input type="hidden" value="two" name="page_id">
                        <label for="exampleInputEmail1" class="form-label form__heading h4 mb-4">Оставить комментарий</label>
                        <textarea type="text" name="comment" class="form-control mb-3 form__input" id="comment" placeholder="Введите комментарий здесь"></textarea>
                        <button type="submit" class="btn btn-primary">Отправить</button>
                    </div>  
                </form>
            </div>
        </div>    
        <section class="mt-5 container" >
            <div class="row" >
                <h1>Комментарии</h1>
                <?php
                    $result = mysqli_query($connect, "SELECT * FROM `comments`");
                    $result = mysqli_fetch_all($result);
                    foreach($result as $item) {
                        if ($item[2] == "two"):
                        ?>
                        <div class="card card-body border-bottom col-3 m-3 p-3"> 
                            <h5 class="card-title">comment</h5>
                            <p class="card-text">Пользователь: <?php echo $item['1'] ?></p>
                            <p class="card-text">Текст: <?php echo $item['3'] ?></p>
                            <p class="card-text">Дата: <?php echo $item['4']?></p>
                            <form method="POST" action="/edit2.php">
                                <input name="edited" type="hidden" value="<?php echo $item['0']?>">
                                <button type="submit" class="btn btn-primary">Редактировать</button>
                            </form>
                        </div>
                        
                <?php
                    endif;
                    }
                ?>
            </div>
        </section>
    </main>
</body>
</html>
<?php
    $connect = mysqli_connect('std-mysql','std_1726_framework', '12345678', 'std_1726_framework');
    $comment_id = $_POST['edited'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
        <p class="h1"><?php $comment_id?></p>
        <div class="container mt-5">
            <div class="row justify-content-center">
                <form class="col-4 text-center" method="POST" action="/update3.php">
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label form__heading h4 mb-4">Редактировать комментарий</label>
                        <input name="comment_id" type="hidden" value="<?php echo $comment_id?>">
                        <textarea name="edited" type="text" class="form-control mb-3 form__input" id="comment" placeholder="Введите комментарий здесь"></textarea>
                        <button type="submit" class="btn btn-primary">Изменить</button>
                    </div>  
                </form>
            </div>
        </div>    
</body>
</html>
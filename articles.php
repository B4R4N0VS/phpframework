<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <header class="border-bottom">
        <div class="container navbar mt-2">
            <p><?=$_COOKIE['user']?></p>
            <a class="btn btn-primary" href="/exit.php">Выход</a>
        </div>
    </header>
    <div class="text-center d-flex justify-content-evenly mt-5 container">
        <div class="card" style="width: 18rem;">
            <img src="img/img1.jpg" class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">Первая статья</h5>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                <a href="/first__article.php" class="btn btn-primary">Подробнее</a>
            </div>
        </div>
        <div class="card" style="width: 18rem;">
            <img src="img/img2.jpg" class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">Вторая статья</h5>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                <a href="/second__article.php" class="btn btn-primary">Подробнее</a>
            </div>
        </div>
        <div class="card" style="width: 18rem;">
            <img src="img/img3.jpg" class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">Третья статья</h5>
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                <a href="/third__article.php" class="btn btn-primary">Подробнее</a>
            </div>
        </div>
    </div>
</body>
</html>